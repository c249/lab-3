import java.io.Serializable;
import java.util.Date;


/**
 * This class represents the Faculty members
 *
 */
public class Faculty extends Member implements Serializable {
	/**
	 * The contract of this Faculty member
	 */
	private Contract contract;
	
	/**Constructor 
	 * @param name the name of the Faculty member
	 * @param contract the member's contract
	 */
	public Faculty(String name, Contract contract){
		super(name);
		this.setContract(contract);
	}

	/**Get the contract
	 * @return contract
	 */
	public Contract getContract() {
		return contract;
	}

	/**Set the contract
	 * @param contract
	 */
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	/**Get the pay of this Faculty member
	 * @return pay
	 */
	public double getPay(Date date){
		double pay = 0;
		//Permanent Pay
		if(getContract().getType() == Contract.Type.Permanent){
			pay = getContract().getMonthlySalary();
		}
		//Part-Time Pay
		else if(getContract().getType() == Contract.Type.PartTime){
			if(!getContract().isValid(date)){
				return 0;
			}
			else if(getContract().isValid(date)){
				pay = getContract().getMonthlySalary() + getContract().getBonus();
			}
		}
		
		return pay;
	}
	
	/** return a string representation of this Faculty member
	 * @return a string representation of this Faculty member
	 */
	public String toString(){
		return ("\nName: " + getName() + "\n" + "Contract: " + contract.getType().toString());
	}
	/** Check for equality between two Faculty member objects
	 * @param obj the other Faculty member object
	 * @return true if and only if the objects are equivalent
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		Faculty mObj = (Faculty)obj; 
		return ( getName().equals(mObj.getName()) && contract.equals(mObj.getContract()));
	}
}
