import java.io.Serializable;
import java.util.Date;

//
/**
 * This class represents the Undergraduate TAs
 */
public class UndergradTA extends Student implements Serializable{
	/**
	 * The hourly rate of undergraduate TAs
	 */
	private static double UhourlyRate;
	/**
	 * The contract of this UndergradTA
	 */
	private Contract contract;
	
	/**Constructor
	 * @param name the name
	 * @param studentID the student id
	 * @param contract the contract
	 */
	public UndergradTA(String name, int studentID, Contract contract){
		super(name,studentID);
		this.setContract(contract);
		
	}
	
	/**Get the salary of this UTA
	 * @return salary the salary
	 */
	public double getSalary(){
		return UhourlyRate * contract.getNumberOfHours();
	}	
	
	
	/** Get the contract
	 * @return contract
	 */
	public Contract getContract() {
		return contract;
	}

	/**Set the contract
	 * @param contract
	 */
	public void setContract(Contract contract) {
		this.contract = contract;
	}

	/**Get the hourly rate for all UTAs
	 * @return UhourlyRate
	 */
	public static double getUhourlyRate() {
		return UhourlyRate;
	}

	/**Set the hourly rate for all UTAs
	 * @param uhourlyRate
	 */
	public static void setUhourlyRate(double uhourlyRate) {
		UhourlyRate = uhourlyRate;
	}
	
	/** Return the name of the UTA
	 * @return the name of the UTA
	 */
	public String toString(){
		return ("\nName: " + getName() + "\nID:" + getStudentID() + "\nContract: "+ contract.getType().toString() + "\n"); 
	}
	/**Checks for equality between two UTAs
	 *@return true if and only if the two UTAs are equal
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		UndergradTA mObj = (UndergradTA)obj; 
		return ( getName().equals(mObj.getName()) && contract.equals(mObj.getContract()));
	}
	/**Get the pay for this UTA member
	 * @param the pay date
	 * @return pay the amount of money that needs to be payed to this UTA member
	 */
	public double getPay(Date date){
		double pay = 0;
		if(!getContract().isValid(date)){
			pay = 0;
		}
		else if(getContract().isValid(date)){
			pay = (getContract().getNumberOfHours() * UhourlyRate) / 4;
		}
		return pay;
	}
}
