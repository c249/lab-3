/*
 * William Prioriello (7080956) and Ivan Bogdev (7053444)
 * COMP 249 section D
 * Lab 3 - Concordia System
 */
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Date;




/**
 * The main class (driver)
 *
 */
public class ConcordiaSystem {
	/**
	 * The number of members in the school
	 */
	static int numberOfMembers ;
	/**
	 * @param args
	 */
	public static void main(String[] args){
		int choice = 0;

		// one scanner for INT and one for STRING
		Scanner inputInt = new Scanner(System.in);
		Scanner inputString = new Scanner(System.in);
		Scanner inputDouble = new Scanner(System.in);
		boolean running = true;

		System.out.println("Welcome to the Concordia Payment System!\n");

		//import the list of members
		//IMPORT THIS
		numberOfMembers = 10;
		//IMPORT THIS1
		Member[] list = new Member[numberOfMembers];

		//import the list of members		
		ObjectInputStream inStream = null;
		try {
			inStream = new ObjectInputStream(new FileInputStream("members"));
			Object objectRead = inStream.readObject();
			if (objectRead != null){
				list = (Member[]) objectRead;				
				numberOfMembers = list.length;
			}
		} catch (FileNotFoundException e) {			
			System.out.println("A new list of members was created.");
			//e.printStackTrace();
		}
		catch (EOFException e){			
			//System.out.println("File was likely never written to");
			//e.printStackTrace();
		}
		catch (IOException e) {			
			e.printStackTrace();
		} catch (ClassNotFoundException e) {			
			e.printStackTrace();
		}

		//OutputStream
		ObjectOutputStream outStream = null;
		try {
			outStream = new ObjectOutputStream(new FileOutputStream("members"));
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (IOException e) {			
			e.printStackTrace();
		}

		Date date = new Date(); //Initializes Today's date
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		while(running){
			//Main menu
			System.out.println("Today's date is: " + dateFormat.format(date) + "\nMain menu:");
			System.out.print("1. Add different individuals\n2. Update attributes of individuals\n3. Delete individuals \n4. Search for an individual and viewing his/her info \n5. List based on a given criteria\n6. Prepare the paystubs of all individuals\n7. Calculate the total amount of money that needs to be paid to all Concordia employees\n8. Check if a person qualifies for a TA position\n9. Advance to the next month\n10. Save and Quit\n");
			System.out.print("Enter your choice: ");
			choice = 0;
			while(choice < 1 || choice > 10){
				choice = inputInt.nextInt();
				if(choice < 1 || choice > 10){
					System.out.print("Invalid choice, Please try again: ");
				}
			}


			switch(choice){

			// -------  ADD DIFFERENT INDIVIDUALS  -------

			case 1: 
				addMember(list);
				//Saving list

				pause();
				break;
			case 2: System.out.println(" -- Update attributes of individuals -- ");
			System.out.print("Enter the name of the individual: ");
			String name = inputString.next();

			Member[] searchResult = search(list, name);

			if (searchResult == null || searchResult.length <1)
				System.out.println("The system did not find an individual with that name");
			else if (searchResult.length == 1){
				String[] attributes = attributesOf(searchResult[0]);
				for (int i =0; i<attributes.length; i++)
					System.out.println((i+1) + ". " + attributes[i]);



				System.out.println("Enter your choice: ");
				int selection = inputInt.nextInt();
				selection--;

				Member m = searchResult[0];
				modifyAttributes(m, attributes[selection], inputString, inputInt, inputString);

			}
			else{
				System.out.println("Multiple matches found: ");
				for(int i = 0; i< searchResult.length; i++){						
					System.out.println((i+1) + "." + searchResult[i].toString());
				}
				System.out.println("Please enter the number associated with the individual you want: ");

				int selection = inputInt.nextInt();
				selection--;
				Member m = searchResult[selection];
				//
				String[] attributes = attributesOf(m);
				for (int i =0; i<attributes.length; i++)
					System.out.println((i+1) + ". " + attributes[i]);

				System.out.println("Enter your choice: ");
				selection = inputInt.nextInt();
				selection--;				


				modifyAttributes(m, attributes[selection], inputString, inputInt, inputString);

			}

			pause();
			break;
			case 3: System.out.println(" -- Delete individuals -- ");
			System.out.print("Enter the name of the individual you want to delete: ");
			String keyword = inputString.next();

			Member[] results = search(list,keyword);

			if (results == null || results.length <1)
				System.out.println("The system did not find an individual with that name");
			else if (results.length == 1){
				eraseMember(list, results[0]);					
				System.out.println("The individual: ");
				System.out.println(results[0].toString());
				System.out.println("Has been deleted");	
			}
			else{
				System.out.println("Multiple matches found: ");
				for(int i = 0; i< results.length; i++){						
					System.out.println((i+1) + "." + results[i].toString());
				}
				System.out.println("Please enter the number associated with the individual to erase them: ");

				int selection = inputInt.nextInt();
				selection--;
				eraseMember(list, results[selection]);

				System.out.println("The individual: ");
				System.out.println(results[selection].toString());
				System.out.println("Has been deleted");
			}


			pause();
			break;
			case 4: System.out.println(" -- Search for an individual -- ");
			//ask for name
			System.out.print("Name: ");
			String search = inputString.nextLine();
			//show results
			int matches = 0;
			for(int i = 0; i < list.length; i++){
				if(list[i] != null && list[i].getName().equals(search)){
					System.out.println(list[i].toString());
					matches = matches + 1;
				}
				else if(i == (list.length - 1) && (list[i] == null || !list[i].getName().equals(search)) && matches == 0){
					System.out.println("No results found matching the name: " + search);
				}
			}
			pause();
			break;

			case 5:	System.out.println(" -- List based on a given criteria -- ");

			String[] choices = new String[]{"All Students", "Faculty", "Staff", "UndergradTA", "GradTA", "Everyone"};
			//Print the choices
			for (int i = 0; i< choices.length; i ++)
				System.out.println((i+1) +". " + choices[i]);
			//


			System.out.print("Enter your choice: ");			

			choice = inputInt.nextInt();


			choice--; // subtract 1 so we can use it as an index for the array
			printMembers(list, inputInt, choice);
			
			pause();
			break;

			case 6: System.out.println(" -- Preparing the paystubs of all individuals -- ");
			//Set TA rates and Gross Sales for staff
			System.out.print("What is the hourly rate for the Undergraduate TAs: ");
			UndergradTA.setUhourlyRate(inputDouble.nextDouble());
			GradTA.setGhourlyRate(UndergradTA.getUhourlyRate() * 1.2);
			System.out.print("What are the gross sales: ");
			Staff.setGrossSales(inputDouble.nextDouble());

			System.out.println(dateFormat.format(date));
			paystubs(list, date);
			pause();
			break;

			case 7: System.out.println(" -- Calculate the total amount of money that needs to be paid to all Concordia employees -- ");
			//Set TA rates and Gross Sales for staff
			System.out.print("What is the hourly rate for the Undergraduate TAs: ");
			UndergradTA.setUhourlyRate(inputDouble.nextDouble());
			GradTA.setGhourlyRate(UndergradTA.getUhourlyRate() * 1.2);
			System.out.print("What are the gross sales: ");
			Staff.setGrossSales(inputDouble.nextDouble());

			double totalPay = calculateTotalPay(list,date);
			System.out.println("The total amount of money needed to pay all employees: " + totalPay);
			pause();
			break;

			case 8: System.out.println(" -- Check if a person qualifies for a TA position -- ");
			System.out.println("Enter the name of the person: ");
			String nameOfStudent = inputString.next();
			Member[] searchResults = search(list, nameOfStudent);
			if(searchResults.length < 1)
				System.out.println("No matches found");
			else if(searchResults.length == 1){
				Member m = searchResults[0];
				if (!(m instanceof Student))
					System.out.println("This person does NOT qualify for a TA position");
				else{
					Student mS = (Student)m;
					if(mS.getCurrentStudent())
						System.out.println("This person does qualify for a TA position");
					else
						System.out.println("This person does NOT qualify for a TA position");

				}
			}
			else if(searchResults.length>1){
				System.out.println("Multiple matches found: " );
				for(int i = 0; i< searchResults.length; i++){						
					System.out.println((i+1) + "." + searchResults[i].toString());
				}
				System.out.println("Please enter the number associated with the individual: ");

				int selection = inputInt.nextInt();
				selection--;

				Member mem = searchResults[selection];
				if (!(mem instanceof Student))
					System.out.println("This person does NOT qualify for a TA position");
				else{
					Student mS = (Student)mem;
					if(mS.getCurrentStudent())
						System.out.println("This person does qualify for a TA position");
					else
						System.out.println("This person does NOT qualify for a TA position");

				}
			}


			pause();
			break;

			case 9: System.out.println(" -- Advancing to the next month -- ");
			date = new Date(date.getYear(),date.getMonth()+1,date.getDate());
			System.out.println("The date is now: " + dateFormat.format(date));
			pause();
			break;	
			case 10:
				System.out.println(" -- Saving --");
			try {
				outStream.writeObject(list);
				inStream.close();
				outStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(" -- Quitting -- \nThank you for using Concordia's Payment System!");
			running = false;
			break;
			}
		}



		

	}
	/** Method used to guide the user through the process of adding any Member
	 * @param list The list of Member objects
	 */
	public static void addMember(Member[] list){
		//scanners
		Scanner inputInt = new Scanner(System.in);
		Scanner inputString = new Scanner(System.in);
		Scanner inputDouble = new Scanner(System.in);

		//beginning of method
		System.out.println("\n -- Add different individuals -- ");
		System.out.println("Do you want to add a \n1. Student\n2. Faculty Member\n3. Staff Member\n4. UndergradTA\n5. GradTA");
		System.out.print("Your choice: ");
		int choice = inputInt.nextInt();

		switch(choice){
		//Adding a Student
		case 1: 
			System.out.print("Student's name: ");
			String name = inputString.nextLine();
			System.out.println("Student Id: ");
			int StudentId = inputInt.nextInt();
			System.out.println("1. Current Student\n2. Alumni");
			boolean current = (inputInt.nextInt() == 1);
			for(int i = 0; i < list.length;i++){
				if(list[i] == null){
					list[i] = new Student(name, StudentId, current);
					System.out.println("Student: " + list[i].toString() + "\nhas been added to the list.");
					break;
				}
			}
			break;
			//Adding Faculty
		case 2: System.out.print("Member's name: ");
		name = inputString.nextLine();
		System.out.println("1. Permanent Faculty\n2. Part-Time Faculty");
		int type = 0;
		while(type < 1 || type > 2 ){
			System.out.println("Your choice: ");
			type = inputInt.nextInt();
		}
		//Permanent
		if(type == 1){
			System.out.print("Yearly Salary: ");
			double salary = inputDouble.nextDouble();

			for(int i = 0; i < list.length;i++){
				if(list[i] == null){
					list[i] = new Faculty(name, new Contract(Contract.Type.Permanent, salary));
					System.out.println("Faculty member: " + list[i].toString() + " has been added to the list.");
					break;
				}
			}
		}
		//Part-Time
		if(type == 2){
			int bonus = 0;
			System.out.print("Number of hours: ");
			double numberOfHours = inputDouble.nextDouble();
			System.out.print("Number of Courses: ");
			int courses = inputInt.nextInt();
			while(courses < 1 || courses > 2){
				System.out.print("Incorrect number of courses, please enter the number of courses again: ");
				courses = inputInt.nextInt();
			}

			System.out.print("number of students in course 1: ");
			int numberOfStudents = inputInt.nextInt();
			while(numberOfStudents < 0){
				System.out.print("Incorrect number of students, please enter the number of students again: ");
				numberOfStudents = inputInt.nextInt();
			}
			if(numberOfStudents >= 40 && numberOfStudents <= 60){
				bonus = 500;
			}
			if(numberOfStudents > 60){
				bonus = 1000;
			}
			if(courses == 2){
				System.out.print("number of students in course 2: ");
				numberOfStudents = inputInt.nextInt();
				while(numberOfStudents < 0){
					System.out.print("Incorrect number of students, please enter the number of students again: ");
					numberOfStudents = inputInt.nextInt();
				}
				if(numberOfStudents >= 40 && numberOfStudents <= 60){
					bonus = bonus + 500;
				}
				if(numberOfStudents > 60){
					bonus = bonus + 1000;
				}
			}
			//calculate salary
			System.out.println("Salary per hour: ");
			double hourlyRate = inputDouble.nextDouble();
			double monthlySalary = (hourlyRate * numberOfHours)/4;

			//start and end dates
			System.out.print("Start date of the contract: \nyear: ");
			int year = inputInt.nextInt() - 1900;
			System.out.print("month: ");
			int month = inputInt.nextInt() - 1;
			System.out.print("day: ");
			int day = inputInt.nextInt();
			Date startDate = new Date(year, month, day);

			System.out.print("End date of the contract: \nyear: ");
			year = inputInt.nextInt() - 1900;
			System.out.print("month: ");
			month = inputInt.nextInt() - 1;
			System.out.print("day: ");
			day = inputInt.nextInt();
			Date endDate = new Date(year, month,day);

			for(int i = 0; i < list.length;i++){
				if(list[i] == null){
					list[i] = new Faculty(name, new Contract(Contract.Type.PartTime, startDate, endDate, monthlySalary,(int)numberOfHours, bonus));
					System.out.println("Faculty member: " + list[i].toString() + " has been added to the list.");
					break;
				}
			}
		}
		break;
		//Adding Staff
		case 3:
			System.out.print("Member's name: ");
			name = inputString.nextLine();
			System.out.println("1. Permanent Staff\n2. Part-Time Staff\n3.Part-Time Staff with Commission");
			type = 0;
			while(!isWithinLimits(type,1,3) ){
				System.out.println("Your choice: ");
				type = inputInt.nextInt();
			}
			//Permanent Staff
			if(type == 1){
				System.out.print("Yearly Salary: ");
				double salary = inputInt.nextInt();
				for(int i = 0; i < list.length; i++){
					if(list[i] == null){
						list[i] = new Staff(name, new Contract(Contract.Type.Permanent, salary));
						System.out.println("Staff: " + list[i].toString() + " has been added to the list");
						break;
					}
				}
			}
			//Part-Time Staff
			if(type == 2){
				//salary
				System.out.print("Number of hours: ");
				double numberOfHours = inputDouble.nextDouble();
				System.out.print("Hourly Rate: ");
				double hourlyRate = inputDouble.nextDouble();
				System.out.print("Monthly Salary: ");
				double monthlySalary = hourlyRate*numberOfHours/4;
				//start and end dates
				System.out.print("Start date of the contract: \nyear: ");
				int year = inputInt.nextInt() - 1900;
				System.out.print("month: ");
				int month = inputInt.nextInt() - 1;
				System.out.print("day: ");
				int day = inputInt.nextInt();
				Date startDate = new Date(year, month, day);
				System.out.print("End date of the contract: \nyear: ");
				year = inputInt.nextInt() - 1900;
				System.out.print("month: ");
				month = inputInt.nextInt() - 1;
				System.out.print("day: ");
				day = inputInt.nextInt();
				Date endDate = new Date(year, month,day);

				//Add to list
				for(int i = 0; i < list.length;i++){
					if(list[i] == null){
						list[i] = new Staff(name, new Contract(Contract.Type.PartTime, startDate, endDate, monthlySalary, (int) numberOfHours));
						System.out.println("Staff: " + list[i].toString() + " has been added to the list");
						break;
					}
				}
			}
			//Part time with commission staff
			if(type == 3){
				//salary
				System.out.println("Yearly Salary: ");
				double monthlySalary = inputDouble.nextDouble()/12;
				System.out.println("Commission percentage of gross sales: ");
				double comission = inputDouble.nextDouble();
				//start and end dates
				System.out.print("Start date of the contract: \nyear: ");
				int year = inputInt.nextInt() - 1900;
				System.out.print("month: ");
				int month = inputInt.nextInt() - 1;
				System.out.print("day: ");
				int day = inputInt.nextInt();
				Date startDate = new Date(year, month, day);
				System.out.print("End date of the contract: \nyear: ");
				year = inputInt.nextInt() - 1900;
				System.out.print("month: ");
				month = inputInt.nextInt() - 1;
				System.out.print("day: ");
				day = inputInt.nextInt();
				Date endDate = new Date(year, month,day);

				//Add to list
				for(int i = 0; i < list.length;i++){
					if(list[i] == null){
						list[i] = new Staff(name, new Contract(Contract.Type.PartTimePlusComission, startDate, endDate, monthlySalary, comission));
						System.out.println("Staff: " + list[i].toString() + " has been added to the list");
						break;
					}
				}
			}

			break;

			//Adding UndergradTA
		case 4: 
			System.out.print("Member's name: ");
			name = inputString.nextLine();
			System.out.println("Student Id: ");
			int studentId = inputInt.nextInt();
			System.out.println("Number of hours: ");
			int numberOfHours = inputInt.nextInt();

			//start and end dates
			System.out.print("Start date of the contract: \nyear: ");
			int year = inputInt.nextInt() - 1900;
			System.out.print("month: ");
			int month = inputInt.nextInt() - 1;
			System.out.print("day: ");
			int day = inputInt.nextInt();
			Date startDate = new Date(year, month, day);
			System.out.print("End date of the contract: \nyear: ");
			year = inputInt.nextInt() - 1900;
			System.out.print("month: ");
			month = inputInt.nextInt() - 1;
			System.out.print("day: ");
			day = inputInt.nextInt();
			Date endDate = new Date(year, month,day);

			for(int i=0;i < list.length;i++){
				if(list[i] == null){
					list[i] = new UndergradTA(name, studentId, new Contract(Contract.Type.PartTime, startDate, endDate, numberOfHours));
					System.out.println("UndergradTA: " + list[i].toString() + " has been added to the list");
					break;
				}
			}

			break;
			//Adding GradTA
		case 5:
			System.out.print("Member's name: ");
			name = inputString.nextLine();
			System.out.println("Student Id: ");
			studentId = inputInt.nextInt();
			System.out.println("Number of hours: ");
			numberOfHours = inputInt.nextInt();

			//start and end dates
			System.out.print("Start date of the contract: \nyear: ");
			year = inputInt.nextInt() - 1900;
			System.out.print("month: ");
			month = inputInt.nextInt() - 1;
			System.out.print("day: ");
			day = inputInt.nextInt();
			startDate = new Date(year, month, day);
			System.out.print("End date of the contract: \nyear: ");
			year = inputInt.nextInt() - 1900;
			System.out.print("month: ");
			month = inputInt.nextInt() - 1;
			System.out.print("day: ");
			day = inputInt.nextInt();
			endDate = new Date(year, month,day);

			for(int i=0;i < list.length;i++){
				if(list[i] == null){
					list[i] = new GradTA(name, studentId, new Contract(Contract.Type.PartTime, startDate, endDate, numberOfHours));
					System.out.println("GradTA: " + list[i].toString() + " has been added to the list");
					break;
				}
			}

			break;
		}
	}

	/** Method used to prepare & display the paystubs of individuals given some Date
	 * @param list The array of Member objects
	 * @param date The date
	 */
	public static void paystubs(Member[] list, Date date){
		Scanner inputDouble = new Scanner(System.in);

		for(int i = 0; i < list.length;i++){
			if((list[i] != null) && list[i].getClass() != Student.class){
				if((list[i].getPay(date) > 0) && list[i].getContract().getType() == Contract.Type.Permanent){
					System.out.println(list[i].toString() + "\n" + "This month's salary: " + list[i].getPay(date));
				}
				else if((list[i].getPay(date) > 0) && list[i].getContract().getType() == Contract.Type.PartTime){
					System.out.println(list[i].toString());
					System.out.println("Number of Hours: " + list[i].getContract().getNumberOfHours());
					if(list[i].getClass() == UndergradTA.class){
						System.out.println("Hourly Rate: " + UndergradTA.getUhourlyRate());
					}
					else if(list[i].getClass() == GradTA.class){
						System.out.println("Hourly Rate: " + GradTA.getGhourlyRate());
					}
					else if(list[i].getClass() != GradTA.class || list[i].getClass() != UndergradTA.class){
						System.out.println("Hourly Rate: " + list[i].getContract().getMonthlySalary()*4/list[i].getContract().getNumberOfHours());
					}
					if(list[i].getClass() == Faculty.class){
						System.out.println("Bonus: " + list[i].getContract().getBonus());
					}
					System.out.println("This month's salary: " + list[i].getPay(date));
				}
				else if((list[i].getPay(date) > 0) && list[i].getContract().getType() == Contract.Type.PartTimePlusComission){
					System.out.println(list[i].toString());
					System.out.println("Monthly Salary: " + list[i].getContract().getMonthlySalary());
					System.out.println("Commission: " + list[i].getContract().getComission() + " %");
					System.out.println("This month's salary: " + list[i].getPay(date));
				}
			}

		}


	}

	/**
	 * Used to pause the flow of execution before going into the next loop iteration
	 */
	public static void pause(){
		Scanner inputString = new Scanner(System.in);
		//Press enter to return to main menu
		System.out.println("Press enter to return to the Main Menu...");
		inputString.nextLine();
	}

	/**This method checks if a number is between a lowerbound and upperbound (inclusive)
	 * @param number The number
	 * @param lowerbound The lower bound
	 * @param upperbound The upper bound
	 * @return true if the number is between the bounds and false otherwise
	 */
	public static boolean isWithinLimits(int number, int lowerbound, int upperbound){
		return (number >= lowerbound && number <= upperbound);
	}

	/**This method is used to delete a member from the Member[] list (We delete the member by setting the reference to null)
	 * @param list The array of members
	 * @param toDelete The member object reference which must be deleted
	 */
	private static void eraseMember(Member[] list, Member toDelete){
		for(int i =0; i < list.length; i ++){
			if(list[i] != null && list[i].equals(toDelete))
				list[i]= null; // erase the member
		}
	}

	/**Get all the attributes of some Member m
	 * @param m The member m
	 * @return a String[] array describing the attributes of the Member m 
	 */
	private static String[] attributesOf(Member m){
		if(m.getClass().getName().equals("GradTA")){
			return new String[]{"Name", "StudentID", "Currently Studying", "Hourly Rate"};
		}
		else if(m.getClass().getName().equals("UndergradTA")){
			return new String[]{"Name", "StudentID", "Currently Studying", "Hourly Rate"};
		}
		else if(m.getClass().getName().equals("Student")){
			return new String[]{"Name", "StudentID", "Currently Studying"};
		}
		else if(m.getClass().getName().equals("Faculty")){
			return new String[]{"Name"};
		}
		else if(m.getClass().getName().equals("Staff")){
			return new String[]{"Name", "Sales"};
		}
		else
			return new String[]{};
	}

	/**Prints some members based user decisions
	 * @param list The array of member objects
	 * @param inputInt a Scanner object (used to input an int)
	 * @param choice between these options "All Students", "Faculty", "Staff", "UndergradTA", "GradTA", "Everyone"
	 * 			where choice = 0 for "All students", choice = 1 for "Faculty" etc.
	 */
	private static void printMembers(Member[] list, Scanner inputInt, int choice){
		switch(choice){
		case 0: // All students
			for (Member m : list){
				if (m != null && (m.getClass().getName().equals("Student") 
						|| m.getClass().getName().equals("GradTA") 
						|| m.getClass().getName().equals("UndergradTA")))						
					System.out.println(m.toString());					
			}
			break;
		case 1: // Faculty
			System.out.println("1." + Contract.Type.Permanent.toString() + " or 2." + Contract.Type.PartTime.toString() + " ?");
			System.out.print("Enter your choice: ");			
			choice = inputInt.nextInt();

			for (Member m : list)
				if (m != null && m.getClass().getName().equals("Faculty")){
					Faculty mF = (Faculty) m;
					if (choice == 1 && mF.getContract().getType().equals(Contract.Type.Permanent))
						System.out.println(mF.toString());

					else if (choice == 2 && mF.getContract().getType().equals(Contract.Type.PartTime))
						System.out.println(mF.toString());
				}

			break;
		case 2: //Staff
			System.out.println("1." + Contract.Type.Permanent.toString() + "or 2." + Contract.Type.PartTime.toString() +" or 3. "
					+ Contract.Type.PartTimePlusComission.toString()  +" ?");

			System.out.print("Enter your choice: ");			
			choice = inputInt.nextInt();
			for (Member m : list)
				if (m != null && m.getClass().getName().equals("Staff")){
					Staff mS = (Staff) m;
					if (choice == 1 && mS.getContract().getType().equals(Contract.Type.Permanent))
						System.out.println(mS.toString());
					else if (choice == 2 && mS.getContract().getType().equals(Contract.Type.PartTime))
						System.out.println(mS.toString());
					else if (choice == 3 && mS.getContract().getType().equals(Contract.Type.PartTimePlusComission))
						System.out.println(mS.toString());
				}
			break;
		case 3: //UndergradTA
			for (Member m : list)
				if (m != null && m.getClass().getName().equals("UndergradTA"))
					System.out.println(m.toString());
			break;
		case 4: //GradTA
			for (Member m : list)
				if (m != null && m.getClass().getName().equals("GradTA"))
					System.out.println(m.toString());
			break;
		case 5://Everyone
			for (Member m : list)
				if (m != null)
					System.out.println(m.getClass().getName() + " " + m.toString() + "\n");
			break;
		}
	}
	
	/**Guides the user through the process of editing member attributes 
	 * @param m The member m we want to edit
	 * @param attribute The attribute we want to edit, can be: "name", "StudentID", or "Currently Studying"
	 * @param inputString Scanner object for inputing strings
	 * @param inputInt Scanner object for inputing ints
	 * @param inputBoolean Scanner object for inputing booleans
	 */
	private static void modifyAttributes(Member m, String attribute, Scanner inputString, Scanner inputInt, Scanner inputBoolean){
		if (attribute.equals("Name")){
			System.out.println("Enter new name: ");
			m.setName(inputString.next());
		}
		else if (attribute.equals("StudentID")){
			System.out.println("Enter new ID: ");
			int id = inputInt.nextInt();
			Student student = (Student) m;
			student.setStudentID(id);
		}
		else if (attribute.equals("Currently Studying")){
			System.out.println("The student is still studying (true/false): ");
			boolean isStillStudying = inputString.nextBoolean();
			Student student = (Student) m;
			student.setCurrentStudent(isStillStudying);
		}
	}


	/**Search for a Member m based on his/her name
	 * @param list The member object array 
	 * @param keyword The name of the Member
	 * @return Two things can happen:
	 * 			1) If No members are found, an empty Member[] array is returned
	 * 			2) If 1 or more members are found, an Member[] array with those members is returned
	 * 			
	 */
	private static Member[] search(Member[] list, String keyword){
		ArrayList<Member> results = new ArrayList<Member>();			
		for(int i = 0; i < list.length; i++){
			if(list[i] != null && list[i].getName().equals(keyword)){
				results.add(list[i]);
			}
			else if(i == (list.length - 1) && (list[i] == null)){
				Member[] arrResults = new Member[results.size()];
				for(int j = 0; j < results.size(); j ++){
					arrResults[j]= results.get(j);
				}
				return arrResults;

			}
		}
		return null; // to keep the compiler happy
	}
	/**Method used to calculate the total amount of money that needs to be payed to all employees of the university
	 * @param list The list of member objects
	 * @param date The date
	 * @return total The total amount of money that needs to be payed
	 */
	private static double calculateTotalPay(Member[] list,Date date){
		double total = 0;

		for(int i = 0; i < list.length; i++){
			if(list[i] != null && list[i].getClass() != Student.class){
				total = total + list[i].getPay(date);
			}
		}

		return total;
	}
}




