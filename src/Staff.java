import java.io.Serializable;
import java.util.Date;


/**
 * This class represents Staff members in the university
 *
 */
public class Staff extends Member implements Serializable{
	/**
	 * The Staff member's contract
	 */
	private Contract contract;
	/**
	 * The total sales by all Staff members
	 */
	private static double grossSales;
	
	/**Constructor
	 * @param name The name of the Staff member
	 * @param contract The contract
	 */
	public Staff(String name, Contract contract){
		super(name);
		this.setContract(contract);
	}

	/**Get the contract
	 * @return contract The contract
	 */
	public Contract getContract() {
		return contract;
	}

	/**Set the contract
	 * @param contract the contract
	 */
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	/** return a string representation of this Staff member
	 * @return a string representation of this Staff member
	 */
	public String toString(){
		return ("\nName: " + getName() + "\n" + "Contract: " + contract.getType().toString());
	}
	
	/**Get the pay for this Staff member
	 * @param date The pay date
	 * @return pay The pay
	 */
	public double getPay(Date date){
		double pay = 0;
		if(getContract().getType() == Contract.Type.Permanent){
			pay = getContract().getMonthlySalary();
		}
		else if(getContract().getType() == Contract.Type.PartTime){
			if(!getContract().isValid(date)){
				return 0;
			}
			else if(getContract().isValid(date)){
				pay = getContract().getMonthlySalary();
			}
		}
		else if(getContract().getType() == Contract.Type.PartTimePlusComission){
			if(!getContract().isValid(date)){
				return 0;
			}
			else if(getContract().isValid(date)){
				pay = getContract().getMonthlySalary() + getContract().getComission()/100 * grossSales;
			}
		}
		return pay;
	}
	
	/** Check for equality between two Staff member objects
	 * @param obj the other Staff member object
	 * @return true if and only if the objects are equivalent
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		Staff mObj = (Staff)obj; 
		return ( getName().equals(mObj.getName()) && contract.equals(mObj.getContract()));
	}

	/**Get the gross sales of all staff members
	 * @return grossSales The grossSales
	 */
	public static double getGrossSales() {
		return grossSales;
	}

	/**Set the gross sales of all staff members
	 * @param grossSales the gross sales
	 */
	public static void setGrossSales(double grossSales) {
		Staff.grossSales = grossSales;
	}
	
}
