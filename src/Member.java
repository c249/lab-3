import java.io.Serializable;
import java.util.Date;


/**This Class is a base class. All Individuals in the University will extend Member.
 *
 *
 */
public abstract class Member implements Serializable{
	/**The name of this Member	 
	 */
	private String name;
	
	/**Default Constructor
	 * 
	 */
	public Member(){
		this.setName("");
	}
	
	/**Set the name of this member
	 * @param name the name of the member
	 */
	public Member(String name){
		this.setName(name);
	}

	/**Get the name of the member
	 * @return name The name of the member
	 */
	public String getName() {
		return name;
	}

	/**Set the name of the member 
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/** Return the name of the member
	 * @return the name of the member
	 */
	public String toString(){
		return name;
	}
	
	/**Abstract method to be implemented well in all members who are getting a salary.
	 * Gets the pay
	 * @param date The pay date
	 * @return should return the pay
	 */
	public abstract double getPay(Date date);
	/**Abstract method to be implemented well in all members who have a Contract
	 * Gets the contract
	 * @return The Contract
	 */
	public abstract Contract getContract();
	
	/**Checks for equality between two members
	 *@return true if and only if the two members are equal
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		Member mObj = (Member)obj; 
		return (this.name.equals(mObj.getName()));
	}
}
