import java.io.Serializable;
import java.util.Date;


/**
 * This class represents the Students in the university
 *
 */
public class Student extends Member implements Serializable{
	/**
	 * The student id number
	 */
	private int studentID;
	/**
	 * Is the student currently studying at the university?
	 */
	private boolean currentStudent;
	
	/**
	 * Default constructor
	 */
	public Student(){
		
	}
	
	/**Constructor
	 * @param name the name
	 * @param studentID the student ID
	 */
	public Student(String name, int studentID){
		super(name);
		this.studentID = studentID; 
		setCurrentStudent(true);
	}
	
	/**Constructor
	 * @param name the name
	 * @param studentID the student ID
	 * @param current is the student currently studying at the university?
	 */
	public Student(String name, int studentID, boolean current){
		super(name);
		this.studentID = studentID;
		currentStudent = current;
	}
	
	/**Get the student id
	 * @return student ID
	 */
	public int getStudentID() {
		return studentID;
	}

	/**
	 * @param studentID
	 */
	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	/**Get if the student is currently still studying
	 * @return currentStudent
	 */
	public boolean isCurrentStudent() {
		return currentStudent;
	}

	/**Set if the student is still studying
	 * @param currentStudent
	 */
	public void setCurrentStudent(boolean currentStudent) {
		this.currentStudent = currentStudent;
	}
	/**Same as isCurrentStudent()
	 * @return currentStudent
	 */
	public boolean getCurrentStudent(){
		return currentStudent;
	}
	/** Return the name of the member
	 * @return the name of the member
	 */
	public String toString(){
		return ("\nName: " + getName()  + "\nID:" + getStudentID() + "\nCurrent Student: " + currentStudent); 
	}
	/**Checks for equality between two Students
	 *@return true if and only if the two Students are equal
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		Student mObj = (Student)obj; 
		return ( getName().equals(mObj.getName()) && currentStudent == mObj.getCurrentStudent());
	}
	/** Get the pay (0 for students)
	 * @return pay The pay
	 */
	public double getPay(Date date){
		return 0;
	}
	/** Get the Contract
	 * @return null for students
	 */
	public Contract getContract(){
		return null;
	}
}
