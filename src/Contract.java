import java.io.Serializable;
import java.util.Date;

/**
 * This class represents the Contracts
 *
 */
public class Contract implements Serializable {
	/**
	 * Contract type
	 */
	private Type type;
	/**
	 * The date at which the contract starts
	 */
	private Date startDate;
	/**
	 * The date at which the contract ends
	 */
	private Date endDate;
	/**
	 * The salary
	 */
	private double salary;
	/**
	 * The salary per month
	 */
	private double monthlySalary;
	/**
	 * The bonus amount for members on a part-time Contract type
	 */
	private int bonus;
	/**
	 * The number of hours in the contract
	 */
	private double numberOfHours;
	/**
	 * The commission (for Staff members)
	 */
	private double comission; 
	
	/**
	 * Enum type representing the different types of contracts 
	 */
	public enum Type{		
		PartTime, 
		FullTime, 
		PartTimePlusComission,
		Permanent,
	}
	
	//Constructors
	/**
	 * Default constructor
	 */
	public Contract(){
		
	}
	/**Constructor
	 * @param type The type of Contract
	 */
	public Contract(Type type){
		this.type = type;
	}
	/**Constructor
	 * @param type The type of Contract
	 * @param salary The salary
	 */
	public Contract(Type type, double salary){
		this.type = type;
		this.salary = salary;
		this.monthlySalary = salary/12;
	}
	
	/**Constructor 
	 * @param type The type of contract
	 * @param startDate The date when the contract starts
	 * @param endDate The date when the contract ends
	 * @param monthlySalary The monthly salary
	 * @param commission The commission
	 */
	public Contract(Type type, Date startDate, Date endDate, double monthlySalary, double commission){
		this.type = type;
		this.monthlySalary = monthlySalary;
		this.startDate = startDate;
		this.endDate = endDate;
		this.comission = commission;
	}
	/**Constructor
	 * @param type The type of contract
	 * @param startDate The date when the contract starts
	 * @param endDate The date when the contract ends
	 * @param monthlySalary The monthly salary
	 * @param numberOfHours The number of working hours in the contract
	 */
	public Contract(Type type, Date startDate, Date endDate, double monthlySalary, int numberOfHours){
		this.type = type;
		this.monthlySalary = monthlySalary;
		this.startDate = startDate;
		this.endDate = endDate;
		this.monthlySalary = monthlySalary;
		this.numberOfHours = numberOfHours;
	}
	/**Constructor
	 * @param type The type of contract
	 * @param startDate The date when the contract starts
	 * @param endDate The date when the contract ends
	 * @param monthlySalary The monthly salary
	 * @param numberOfHours The number of working hours in the contract
	 * @param bonus The bonus
	 */
	public Contract(Type type, Date startDate, Date endDate, double monthlySalary, int numberOfHours, int bonus){
		this.type = type;
		this.startDate = startDate;
		this.endDate = endDate;
		this.monthlySalary = monthlySalary;
		this.numberOfHours = numberOfHours;
		this.setBonus(bonus);
	}
	/**Constructor
	 * @param type The type of contract
	 * @param startDate The date when the contract starts
	 * @param endDate The date when the contract ends
	 * @param numberOfHours The number of working hours in the contract
	 */
	public Contract(Type type, Date startDate, Date endDate, int numberOfHours){
		this.type = type;
		this.startDate = startDate;
		this.endDate = endDate;
		this.numberOfHours = numberOfHours;
	}
	
	
	//Getter And Setter methods
	/**Return the starting date of the contract
	 * @return startDate
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**Set the starting date of the contract
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	/**Return the ending date of the contract
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**Set the ending date of the contract
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**return the salary
	 * @return salary
	 */
	public double getSalary() {
		return salary;
	}
	/**Set the salary
	 * @param salary the salary
	 */
	public void setSalary(double salary) {
		this.salary = salary;
	}
	/**Get the monthly salary
	 * @return monthlySalary
	 */
	public double getMonthlySalary() {
		return monthlySalary;
	}
	/**Set the monthly salary
	 * @param monthlySalary
	 */
	public void setMonthlySalary(double monthlySalary) {
		this.monthlySalary = monthlySalary;
	}
	/**Get the contract type
	 * @return type
	 */
	public Type getType() {
		return type;
	}
	/**Set the contract type
	 * @param type
	 */
	public void setType(Type type) {
		this.type = type;
	}
	/**return the number of hours in the contract
	 * @return numberOfHours
	 */
	public double getNumberOfHours() {
		// TODO Auto-generated method stub
		return numberOfHours;
	}
	/**Set the number of hours in the contract
	 * @param numOfHours
	 */
	public void setNumberOfHours(double numOfHours) {
		// TODO Auto-generated method stub
		this.numberOfHours = numOfHours;
	}
	
	/** Check for equality between two Contracts
	 * 
	 * @param obj The other contract object
	 * @return true if the contracts are equal and false otherwise
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		Contract mObj = (Contract)obj;
		return (type.equals(mObj.getType()) && startDate.equals(mObj.getStartDate()) && endDate.equals(mObj.getEndDate())
				&& salary == mObj.getSalary() && monthlySalary == mObj.getMonthlySalary() );
	}
	/**Check if the contract is expired
	 * @param date Usually the current date
	 * @return true if the contract has expired and false otherwise
	 */
	public boolean isValid(Date date){
		if(date.before(endDate)){
			return true;
		}
		else if(date.after(endDate)){
			return false;
		}
		return false;
	}
	
	/**Get the bonus
	 * @return bonus
	 */
	public int getBonus() {
		return bonus;
	}
	/**Set the bonus
	 * @param bonus
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}
	/**Get the commission
	 * @return commission
	 */
	public double getComission() {
		return comission;
	}
	/**Set the commission
	 * @param comission
	 */
	public void setComission(double comission) {
		this.comission = comission;
	}
	
}
