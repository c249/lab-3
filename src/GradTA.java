import java.io.Serializable;
import java.util.Date;

//
/**
 * This class represents the Graduate TAs
 *
 */
public class GradTA extends Student implements Serializable{
	/**
	 * The hourly rate payed to a Graduate TA
	 */
	private static double GhourlyRate;
	/**
	 * The contract of this Graduate TA
	 */
	private Contract contract;
	
	
	/**Constructor
	 * @param name the name of the GradTA
	 * @param studentID his ID number
	 * @param contract his contract
	 */
	public GradTA(String name, int studentID, Contract contract){
		super(name,studentID);
		this.setContract(contract);
	}

	/**Returns the salary of this GradTA
	 * @return salary The salary
	 */
	public double getSalary(){
		return GhourlyRate * contract.getNumberOfHours();
	}
	
	/**Return the contract of this GradTA 
	 * @return contract the contract
	 */
	public Contract getContract() {
		return contract;
	}

	/**Set the GradTA's contract
	 * @param contract the contract
	 */
	public void setContract(Contract contract) {
		this.contract = contract;
	}

	/**Get the hourly rate payed to any GradTA
	 * @return GhourlyRate
	 */
	public static double getGhourlyRate() {
		return GhourlyRate;
	}

	/**Set the hourly rate of a GradTA
	 * @param ghourlyRate the hourly rate
	 */
	public static void setGhourlyRate(double ghourlyRate) {
		GhourlyRate = ghourlyRate;
	}
	
	/**Return a reasonable String representation of a GradTA
	 * @return a reasonable String representation of a GradTA
	 */
	public String toString(){
		return ("\nName: " + getName()  + "\nID:" + getStudentID() + "\nContract: "+ contract.getType().toString() + "\n"); 
	}
	/** Check for equality between two GradTAs
	 * 
	 * @param obj The other GradTA object
	 * @return true if the GradTAs are equal and false otherwise
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj){
		if (obj == null || (obj.getClass() != this.getClass()))
			return false;
		GradTA mObj = (GradTA)obj; 
		return ( getName().equals(mObj.getName()) && contract.equals(mObj.getContract()));
	}
	/**Get the pay for this GradTA member
	 * @param the pay date
	 * @return pay the amount of money that needs to be payed to this GradTA member
	 */
	public double getPay(Date date){
		double pay = 0;
		if(!getContract().isValid(date)){
			pay = 0;
		}
		else if(getContract().isValid(date)){
			pay = (getContract().getNumberOfHours() * GhourlyRate) / 4;
		}
		return pay;
	}
}
